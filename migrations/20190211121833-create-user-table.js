'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      firstName: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false,
      },
      lastName: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false,
      },
      phoneNumber: {
        type: Sequelize.STRING,
        defaultValue: '',
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        defaultValue: '',
      },
      dateOfBirth: {
        type: Sequelize.DATEONLY,
      },
      rating: {
        type: Sequelize.FLOAT,
        defaultValue: 0.0,
      },
      role: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      photoId: {
        type: Sequelize.INTEGER,
      },
      subscribers: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      createdAt: {
        type: Sequelize.DATE,
      },
    })
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};
