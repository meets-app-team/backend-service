import { Request } from 'express';

export default {
  '/': {
    GET: (req: Request) => req.validationErrors(),
  },
  '/test': {
    GET: (req: Request) => req.validationErrors(),
  },
};
