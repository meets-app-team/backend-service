
import { Router } from 'express';
const router = Router();
import indexController from '../controllers/index.controller';

router.get('/', indexController);

export default router;
