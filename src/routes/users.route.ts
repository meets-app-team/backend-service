import { Router } from 'express';
const router = Router();
import { postCreate } from '../controllers/user.controller';

router.post('/', postCreate);

export default router;
