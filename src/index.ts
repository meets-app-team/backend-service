import './envs';
import express, { Request, Response, NextFunction } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import expressValidator from 'express-validator';
import { json, urlencoded } from 'body-parser';
import { serve, setup } from 'swagger-ui-express';
import { readdirSync } from 'fs';
import validatorController from './controllers/validator.controller';
import decryptController from './controllers/decrypt.controller';
import 'express-async-errors';

const app = express();
import swagger from './utils/swagger';

app.use(cors());
app.use(morgan('dev'));
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(expressValidator());
app.use(decryptController);
app.use(validatorController);

readdirSync(`${__dirname}/routes`)
  .forEach((file: string) => {
    if (!file.includes('.map') && file.includes('index')) {
      return app.use('/', require(`${__dirname}/routes/${file}`).default);
    }

    if (!file.includes('.map') && !file.includes('index')) {
      const fileEnding = file.includes('.route.js') ? '.route.js' : '.route.ts';
      return app.use(
        `/${file.replace(fileEnding, '')}`,
        require(`${__dirname}/routes/${file}`).default);
    }
  });

app.use('/api-docs', serve, setup(swagger));
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  if (err.message) {
    res.status(403);
    res.json({ error: err.message });
  }

  next(err);
});

export default app;
