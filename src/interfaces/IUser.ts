export interface IUserModel {
  id?: number;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  email?: string;
  dateOfBirth?: string;
  rating?: number;
  role?: number;
  photoId?: number;
  subscribers?: number;
}
