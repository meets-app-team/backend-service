import sequelize from './';
import { IUserModel } from '../interfaces/IUser';

export default class User {
  public static async create(user: IUserModel): Promise<IUserModel> {
    let createdUser;
    try {
      createdUser = await sequelize.models.Users.create(user);
    } catch (e) {
      throw new Error('User with this phone has been already created.');
    }

    return createdUser;
  }

  public static async getAll(): Promise<IUserModel[]> {
    return await sequelize.models.Users.findAll({
      attributes: ['id', 'firstName', 'lastName', 'phoneNumber'],
      raw: true,
    });
  }
}
