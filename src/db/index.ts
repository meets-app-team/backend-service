import { Sequelize } from 'sequelize-typescript';
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../../config/config.json`)[env];
const sequelize = new Sequelize(config);

sequelize.addModels([`${__dirname}/../models/*`]);

export default sequelize;
