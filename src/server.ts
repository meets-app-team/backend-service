import http from 'http';
import application from './';
import Logger from './services/logger.service';

const server = http.createServer(application);
const host: any = process.env.HOST || 'localhost';
const port = process.env.PORT || 3000;

server.on('error', (err) => {
  Logger.error('error appears');
  Logger.error(err);
  process.exit(1);
});

process
  .on('unhandledRejection', (reason, p) => {
    Logger.error(`${reason} Unhandled Rejection at Promise ${p}`);
  })
  .on('uncaughtException', (err) => {
    Logger.error(err);
    process.exit(1);
  });

server.listen(port, host, () => {
  Logger.info(`Server listen on ${host}:${port}`);
});
