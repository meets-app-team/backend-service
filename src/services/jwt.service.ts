import jwt from 'jsonwebtoken';

class JWTService {
  public static generateJWT(id: any) {
    const token = jwt.sign({ id }, process.env.ACCESS_SECRET, {
      expiresIn: '1h',
    });
    const refreshToken = jwt.sign({ id }, process.env.REFRESH_SECRET, {
      expiresIn: '7d',
    });
    const accessTokenExpiredAt = (jwt.decode(token) as any).exp;

    return {
      token, refreshToken, accessTokenExpiredAt,
    };
  }

  public static verify(data: string) {
    return jwt.verify(data, process.env.ACCESS_SECRET);
  }
}

export default JWTService;
