import { IUserModel } from '../interfaces/IUser';
import CryptoService from './crypto.service';
import JWTService from './jwt.service';
import UserDB from '../db/user.db';
import Redis from './redis.service';
import { IJWT } from '../interfaces/IJWT';

class UserService {
  public static async create(user: IUserModel): Promise<IJWT> {
    const createdUser = await UserDB.create(user);
    const jwt = JWTService.generateJWT(createdUser.id);

    Redis.setUserToken(createdUser.id, jwt.token);

    jwt.refreshToken = CryptoService.encrypt(jwt.refreshToken);
    jwt.token = CryptoService.encrypt(jwt.token);

    return jwt;
  }
}

export default UserService;
