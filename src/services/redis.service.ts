import redis from 'redis';
import Logger from './logger.service';

const client = redis.createClient(process.env.REDIS_URL);

client.on('error', (err) => {
  Logger.error(err);
});

class RedisService {
  static setUserToken(userId: number, token: string) {
    return client.set(userId.toString(), token, redis.print);
  }

  static async getTokenAsync(id: any) {
    return new Promise((resolve, reject) => {
      client.get(id, (err, reply) => {
        if (err) {
          return reject(err);
        }

        return resolve(reply);
      });
    });
  }
}

export default RedisService;
