import * as logger from 'winston';

class Logger {
  info(msg: any) {
    logger.info(msg);
  }

  error(msg: any) {
    logger.error(msg);
  }
}

export default new Logger();
