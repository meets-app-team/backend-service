import { readFileSync } from 'fs';
import { config } from 'dotenv';

config();

const requiredEnv = readFileSync('.env.example', 'utf-8')
  .split('\n')
  .map((item: string) => item.split('=')[0])
  .filter((env: string) => env !== '');

const unsetEnv = requiredEnv.filter((env: string) =>
  (!(typeof process.env[env] !== 'undefined') || !(process.env[env] !== '')));

if (unsetEnv.length > 0) {
  throw new Error(`Required ENV variables are not set: [${unsetEnv.join(', ')}]`);
}
