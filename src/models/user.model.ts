import { Table, Column, Model, Default, AllowNull, DataType, Unique } from 'sequelize-typescript';

@Table
export default class Users extends Model<Users> {
  @Default('')
  @AllowNull(false)
  @Column
  firstName: string;

  @Default('')
  @AllowNull(false)
  @Column
  lastName: string;

  @AllowNull(false)
  @Unique
  @Column
  phoneNumber: string;

  @Default('')
  @Column
  email: string;

  @Column(DataType.DATEONLY)
  dateOfBirth: string;

  @Default(0.0)
  @Column(DataType.FLOAT)
  rating: number;

  @Default(0)
  @Column
  role: number;

  @Column
  photoId: number;

  @Default(0)
  @Column
  subscribers: number;
}
