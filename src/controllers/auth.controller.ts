
import { INTERNAL_SERVER_ERROR, NOT_ACCEPTABLE } from 'http-status-codes';
import JWTService from '../services/jwt.service';
import RedisService from '../services/redis.service';
import { Request, Response, NextFunction } from 'express';

export default async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.headers.authorization) {
      return res.status(NOT_ACCEPTABLE).send('Need authorization token in header');
    }

    const token = req.headers.authorization.replace('Bearer ', '');

    const verified: any = JWTService.verify(token);
    const redisToken: any = await RedisService.getTokenAsync(verified.id);

    const redisVerified: any = JWTService.verify(redisToken);

    if (verified.id === redisVerified.id) {
      return next();
    }

    return res.status(NOT_ACCEPTABLE).json({ message: 'Session not found' });
  } catch (error) {
    return res.status(INTERNAL_SERVER_ERROR).json({ message: error.message });
  }
};
