import CryptoService from '../services/crypto.service';
import { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction) => {
  if (req.headers.authorization) {
    req.headers.authorization = CryptoService.decrypt(req.headers.authorization);
  }

  return next();
};
