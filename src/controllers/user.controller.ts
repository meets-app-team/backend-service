import UserService from '../services/user.service';
import { Request, Response } from 'express';
import { OK } from 'http-status-codes';

export const postCreate = async (req: Request, res: Response) =>
  res.json({ code: OK, result: await UserService.create(req.body) });
