import { OK } from 'http-status-codes';
import indexService from '../services/index.service';
import { Request, Response } from 'express';

export default (req: Request, res: Response) => {
  const message: String = indexService.index();

  return res.json({
    message,
    code: OK,
  });
};
