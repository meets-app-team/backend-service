import validator from '../utils/validator';
import { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction) => {
  const { method, path } = req;
  const validatorPath = (validator as any)[path];
  if (validatorPath) {
    const validatorMethod = (validatorPath as any)[method];
    if (validatorMethod) {
      const errors = validatorMethod(req);
      if (errors) {
        return res.status(400).send(errors[0]);
      }
    }
  }

  return next();
};
