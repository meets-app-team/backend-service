FROM node:carbon

COPY . /var/www

WORKDIR /var/www

ENV HOST="0.0.0.0"

RUN npm install

EXPOSE 3000
ENV NODE_ENV="production"

ENTRYPOINT [ "npm", "start" ]