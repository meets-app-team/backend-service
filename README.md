# Branching strategy

We're developing our features and bug fixing in feature branches based off master branch (also known as topic branches). Feature branches isolate work in progress from the completed work in the master branch.

![Image](https://cdn-images-1.medium.com/max/1600/1*9X8g9UixIyngytvW_97z6A.png)

Here's the way you should name your branches: `feature/description` or `bugfix/description`, depending on issue type.

Where:
* `feature` - new feature
* `bugfix` - changes linked to a known issue
* `description` - description should be your issue number

**Workflow**

* Use feature branches for all new features and bug fixes.
* Merge feature branches into the master branch using pull requests.
* Keep master branch up-to-date.

Git commands:

`git checkout -b feature/{branch_name}` - to create new branch based on current
